<!-- 
    SPDX-FileCopyrightText: (c) 2020 Datraverse BV <info@datraverse.com> 
    SPDX-License-Identifier: AGPL-3.0-or-later 
-->

## 0.1.0 (September 14, 2019)
- Initial release