/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author Rijk Ravestein
 */
public final class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPrinterDialog(null);
            }
        });

        this.refreshPrinterList();
    }

    public void refreshPrinterList() {

        this.recyclerView = findViewById(R.id.list);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final PrinterRecyclerViewAdapter adapter =
                new PrinterRecyclerViewAdapter(PrintApp.getPrinterList(),
                        new PrinterFragment.OnListFragmentInteractionListener() {
                            @Override
                            public void onListFragmentInteraction(
                                    PrinterItem item) {
                                showPrinterDialog(item);
                            }
                        });

        this.recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Shows the printer details.
     *
     * @param item
     *            {@code null} if adding a printer.
     */
    private void showPrinterDialog(PrinterItem item) {
        final DialogFragment fragment = new PrinterDialogFragment(item);
        fragment.show(this.getSupportFragmentManager(), "org.savapage.printer");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        case R.id.action_add_printer:
            this.showPrinterDialog(null);
            this.refreshPrinterList();
            return true;

        case R.id.action_settings:
            final DialogFragment settingsFragment =
                    new SettingsDialogFragment();
            settingsFragment.show(this.getSupportFragmentManager(),
                    "org.savapage.settings");
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    }

}
