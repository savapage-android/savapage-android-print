/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Rijk Ravestein
 */
public final class PrintApp extends Application {

    private static final String PREF_KEY_PRINTERS = "printers";
    private static final String PREF_KEY_TRUST_SELF_SIGNED = "trustSelfSigned";

    private static PrintApp instance;

    private static final String SHARED_PREFS_NAME = "application";

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static SharedPreferences getSharedPreferences() {
        return getContext().getSharedPreferences(SHARED_PREFS_NAME,
                Context.MODE_PRIVATE);
    }

    public static boolean isTrustSelfSigned() {
        return getSharedPreferences().getBoolean(PREF_KEY_TRUST_SELF_SIGNED,
                false);
    }

    public static void setTrustSelfSigned(final boolean trust) {
        getSharedPreferences().edit()
                .putBoolean(PREF_KEY_TRUST_SELF_SIGNED, trust).apply();
    }

    public static HashMap<String, PrinterItem> getPrinters() {
        final String json =
                getSharedPreferences().getString(PREF_KEY_PRINTERS, "{}");
        final Gson gson = new Gson();
        final Type type = new TypeToken<HashMap<String, PrinterItem>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static List<PrinterItem> getPrinterList() {
        return new ArrayList<>(getPrinters().values());
    }

    public static PrinterItem getPrinter(final String name) {
        return getPrinters().get(name);
    }

    /**
     * Sets printer.
     *
     * @param oldName
     *            Old printer name, can be {@code null} for new printer.
     * @param item
     *            Printer.
     */
    public static void setPrinter(final String oldName,
            final PrinterItem item) {

        final HashMap<String, PrinterItem> map = getPrinters();

        if (oldName != null) {
            map.remove(oldName);
        }
        map.put(item.getName(), item);

        final Gson gson = new Gson();
        final String json = gson.toJson(map);

        getSharedPreferences().edit().putString(PREF_KEY_PRINTERS, json)
                .apply();
    }

    public static void removePrinter(final String name) {

        final HashMap<String, PrinterItem> map = getPrinters();
        map.remove(name);
        final Gson gson = new Gson();
        final String json = gson.toJson(map);

        getSharedPreferences().edit().putString(PREF_KEY_PRINTERS, json)
                .apply();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
