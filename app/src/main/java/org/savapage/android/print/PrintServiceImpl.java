/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.content.Context;
import android.content.SharedPreferences;
import android.printservice.PrintJob;
import android.printservice.PrintService;
import android.printservice.PrinterDiscoverySession;
import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Rijk Ravestein
 */
public final class PrintServiceImpl extends PrintService {

    private final static String LOG_TAG =
            PrintServiceImpl.class.getSimpleName();

    private final static int READ_BUFFER_SIZE = 1024;

    @Override
    protected PrinterDiscoverySession onCreatePrinterDiscoverySession() {
        Log.d(LOG_TAG, "onCreatePrinterDiscoverySession()");

        final SharedPreferences prefs =
                this.getApplication().getApplicationContext()
                        .getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

        return new PrinterDiscoverySessionImpl(this);
    }

    @Override
    protected void onRequestCancelPrintJob(final PrintJob job) {
        Log.d(LOG_TAG,
                "onRequestCancelPrintJob(printer id = "
                        + job.getInfo().getPrinterId().getLocalId()
                        + ", job id = " + job.getId());
    }

    @Override
    protected void onPrintJobQueued(final PrintJob job) {
        Log.d(LOG_TAG,
                "onPrintJobQueued(printer id = "
                        + job.getInfo().getPrinterId().getLocalId()
                        + ", job id = " + job.getId());

        job.start();

        File mTmp = new File(getExternalCacheDir(),
                "android-" + UUID.randomUUID().toString() + ".pdf");

        Log.d(LOG_TAG, "save tmp file:" + mTmp.getAbsolutePath());

        try (FileOutputStream fos = new FileOutputStream(mTmp);
                FileInputStream fin = new FileInputStream(
                        job.getDocument().getData().getFileDescriptor());) {
            Log.d(LOG_TAG, "copy start");
            int c;
            final byte[] bytes = new byte[READ_BUFFER_SIZE];

            while ((c = fin.read(bytes)) != -1) {
                fos.write(bytes, 0, c);
                fos.flush();
            }
            Log.d(LOG_TAG, "copy end");

            fos.close();
            fin.close();

            this.uploadFile(job, mTmp);

        } catch (Exception e) {
            e.printStackTrace();
            job.fail(e.getMessage());
            mTmp.delete();
        } finally {
            Log.d(LOG_TAG, "end-of-print");
        }
    }

    private String createAuthToken(final String user, final String pw) {
        byte[] data = new byte[0];
        try {
            data = (user + ":" + pw).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    private void uploadFile(final PrintJob job, final File file)
            throws IOException {

        // create RequestBody instance from file
        final RequestBody requestFile =
                RequestBody.create(MediaType.get("application/pdf"), file);

        final String jobName = String.format("android-%s",
                job.getDocument().getInfo().getName());

        // MultipartBody.Part is used to send also the actual file name
        final MultipartBody.Part body = MultipartBody.Part
                .createFormData("file", file.getName(), requestFile);

        // add another part within the multipart request
        final String descriptionString = jobName;
        final RequestBody description = RequestBody
                .create(okhttp3.MultipartBody.FORM, descriptionString);

        final PrinterItem printer =
                PrintApp.getPrinter(job.getInfo().getPrinterId().getLocalId());

        final String userid = printer.getUserId();
        final String uuid = printer.getUuid().toString();

        // create upload service client
        final RESTService service = RESTServiceFactory.create(RESTService.class,
                printer.getUrl(), PrintApp.isTrustSelfSigned());

        final Call<ResponseBody> call = service
                .upload(this.createAuthToken(userid, uuid), description, body);

        Log.d(LOG_TAG, "uploading...");

        if (false) {
            final Response<ResponseBody> rsp = call.execute();
            if (rsp.isSuccessful()) {
                Log.d(LOG_TAG, "uploaded OK");
            } else {
                Log.w(LOG_TAG, "upload error: " + rsp.message());
            }
        } else {
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call,
                        Response<ResponseBody> response) {
                    Log.v(LOG_TAG, "Upload success");
                    job.complete();
                    file.delete();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(LOG_TAG, "Upload error: " + t.getMessage());
                    job.fail(t.getMessage());
                    file.delete();
                }
            });
        }
    }
}
