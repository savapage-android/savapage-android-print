/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import java.net.URL;
import java.util.UUID;

/**
 * @author Rijk Ravestein
 */
public final class PrinterDialogFragment extends DialogFragment {

    private final static String LOG_TAG =
            PrinterDialogFragment.class.getSimpleName();

    private final PrinterItem printerItem;

    public PrinterDialogFragment() {
        this.printerItem = null;
    }

    public PrinterDialogFragment(final PrinterItem item) {
        this.printerItem = item;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        final View viewInflated = LayoutInflater.from(this.getContext())
                .inflate(R.layout.edit_printer, (ViewGroup) getView(), false);

        if (this.printerItem != null) {

            TextView txtView;

            txtView = viewInflated.findViewById(R.id.name);
            txtView.setText(this.printerItem.getName());

            txtView = viewInflated.findViewById(R.id.url);
            txtView.setText(this.printerItem.getUrl().toString());

            txtView = viewInflated.findViewById(R.id.userid);
            txtView.setText(this.printerItem.getUserId());

            txtView = viewInflated.findViewById(R.id.uuid);
            txtView.setText(this.printerItem.getUuid().toString());
        }
        //
        builder.setView(viewInflated);

        // Note: don't use setMessage(). If used, ...
        builder.setTitle(R.string.printer_label)
                .setPositiveButton(R.string.save_button,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                    int id) {

                                final PrinterItem item = new PrinterItem();
                                TextView txtView;

                                txtView = viewInflated.findViewById(R.id.name);
                                item.setName(txtView.getText().toString());

                                txtView = viewInflated.findViewById(R.id.url);
                                try {
                                    item.setUrl(new URL(
                                            txtView.getText().toString()));
                                } catch (Exception e) {
                                    // TODO
                                }

                                txtView =
                                        viewInflated.findViewById(R.id.userid);
                                item.setUserId(txtView.getText().toString());

                                txtView = viewInflated.findViewById(R.id.uuid);
                                item.setUuid(UUID.fromString(
                                        txtView.getText().toString()));

                                String oldName = null;
                                if (printerItem != null) {
                                    oldName = printerItem.getName();
                                }
                                PrintApp.setPrinter(oldName, item);

                                refreshActivity();
                            }
                        })
                .setNegativeButton(R.string.delete_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int id) {
                                PrintApp.removePrinter(printerItem.getName());
                                refreshActivity();
                            }
                        })
                .setNeutralButton(R.string.cancel_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int id) {
                                // User cancelled the dialog
                            }
                        });

        return builder.create();
    }

    private void refreshActivity() {
        final Activity activity = getActivity();
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}
