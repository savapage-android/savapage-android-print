/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.print.PrintAttributes;
import android.print.PrinterCapabilitiesInfo;
import android.print.PrinterId;
import android.print.PrinterInfo;
import android.printservice.PrintService;
import android.printservice.PrinterDiscoverySession;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rijk Ravestein
 */
public final class PrinterDiscoverySessionImpl extends PrinterDiscoverySession {
    private final static String LOG_TAG =
            PrinterDiscoverySessionImpl.class.getSimpleName();

    private PrintService printService;

    public PrinterDiscoverySessionImpl(final PrintService service) {
        this.printService = service;
    }

    @Override
    public void onStartPrinterDiscovery(final List<PrinterId> list) {

        Log.d(LOG_TAG, "onStartPrinterDiscovery()");
        for (PrinterId id : list) {
            Log.d(LOG_TAG, "printerId:" + id.getLocalId());
        }

        final List<PrinterInfo> printers = new ArrayList<PrinterInfo>();

        for (PrinterItem item : PrintApp.getPrinterList()) {

            Log.d(LOG_TAG, "Add Printer: " + item.getName());

            final PrinterId printerId =
                    this.printService.generatePrinterId(item.getName());

            final PrinterCapabilitiesInfo.Builder capBuilder =
                    new PrinterCapabilitiesInfo.Builder(printerId);

            capBuilder.addMediaSize(PrintAttributes.MediaSize.ISO_A4, true);
            capBuilder.addMediaSize(PrintAttributes.MediaSize.ISO_B5, false);

            capBuilder.addResolution(new PrintAttributes.Resolution("Default",
                    "default resolution", 600, 600), true);

            capBuilder.setColorModes(
                    PrintAttributes.COLOR_MODE_COLOR
                            | PrintAttributes.COLOR_MODE_MONOCHROME,
                    PrintAttributes.COLOR_MODE_COLOR);

            final PrinterInfo.Builder infoBuilder = new PrinterInfo.Builder(
                    printerId, item.getName(), PrinterInfo.STATUS_IDLE);

            infoBuilder.setCapabilities(capBuilder.build());
            printers.add(infoBuilder.build());
        }

        this.addPrinters(printers);
    }

    @Override
    public void onStopPrinterDiscovery() {
        Log.d(LOG_TAG, "onStopPrinterDiscovery()");
    }

    @Override
    public void onValidatePrinters(final List<PrinterId> list) {
        Log.d(LOG_TAG, "onValidatePrinters()");
        for (PrinterId id : list) {
            Log.d(LOG_TAG, "printerId:" + id.getLocalId());
        }
    }

    @Override
    public void onStartPrinterStateTracking(final PrinterId printerId) {
        Log.d(LOG_TAG, "onStartPrinterStateTracking(printerId: "
                + printerId.getLocalId() + ")");
    }

    @Override
    public void onStopPrinterStateTracking(final PrinterId printerId) {
        Log.d(LOG_TAG, "onStopPrinterStateTracking(printerId: "
                + printerId.getLocalId() + ")");
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "onDestroy()");
    }

}
