/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import java.net.URL;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Rijk Ravestein
 */
public final class RESTServiceFactory {

    private static final String BASE_URL_PATH = "/restful/v1/documents/";

    /**
     * @param serviceClass
     * @param url
     *            REST service URL (protocol://host:port)
     * @param trustSelfSigned
     * @param <S>
     * @return
     */
    public static <S> S create(final Class<S> serviceClass, final URL url,
            final boolean trustSelfSigned) {

        final Retrofit.Builder builder =
                new Retrofit.Builder().baseUrl(url.toString() + BASE_URL_PATH)
                        .addConverterFactory(GsonConverterFactory.create());

        if (trustSelfSigned) {
            builder.client(TrustAllHttpClient.getClient());
        }
        return builder.build().create(serviceClass);
    }
}
