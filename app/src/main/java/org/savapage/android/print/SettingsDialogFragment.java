/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

/**
 * @author Rijk Ravestein
 */
public final class SettingsDialogFragment extends DialogFragment {

    private ArrayList<Integer> selectedItems;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        selectedItems = new ArrayList<>(); // Where we track the selected items

        final CharSequence[] items =
                getResources().getTextArray(R.array.settings_choice_items);

        boolean[] checkedItems = new boolean[1];
        checkedItems[0] = PrintApp.isTrustSelfSigned();

        final AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        builder.setMultiChoiceItems(items, checkedItems,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                            boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the
                            // selected
                            // items
                            selectedItems.add(which);
                        } else if (selectedItems.contains(which)) {
                            // Else, if the item is already in the array, remove
                            // it
                            selectedItems.remove(Integer.valueOf(which));
                        }
                    }
                });

        // Note: don't use setMessage(). If used, MultiChoiceItems are NOT
        // shown.
        builder.setTitle(R.string.action_settings).setPositiveButton(
                R.string.save_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PrintApp.setTrustSelfSigned(!selectedItems.isEmpty());
                    }
                }).setNegativeButton(R.string.cancel_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int id) {
                                // User cancelled the dialog
                            }
                        });

        return builder.create();
    }
}
