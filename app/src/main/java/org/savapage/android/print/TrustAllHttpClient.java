/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.android.print;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

/**
 * @author Rijk Ravestein
 */
public final class TrustAllHttpClient {

    /**
     * X509 Trust manager that does not validate certificate chains.
     */
    private static final TrustManager MANAGER_X509_TRUST_ALL =
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }
            };

    /**
     * Trust manager that does not validate certificate chains.
     */
    private static final TrustManager[] TRUST_ALL_CERTS =
            new TrustManager[] { MANAGER_X509_TRUST_ALL };

    private static OkHttpClient allTrustClient;

    static {

        try {
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, TRUST_ALL_CERTS, new SecureRandom());

            final SSLSocketFactory sslSocketFactory =
                    sslContext.getSocketFactory();

            final OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.sslSocketFactory(sslSocketFactory,
                    (X509TrustManager) TRUST_ALL_CERTS[0]);

            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            allTrustClient = builder.build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private TrustAllHttpClient() {
    }

    /**
     * Creates HTTP Client that trusts all certs and does not check if host name
     * matches the certificate.
     *
     * @return HTTP Client.
     */
    public static OkHttpClient getClient() {
        return allTrustClient;
    }
}
